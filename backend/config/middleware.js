module.exports = {
    "timeout": 100,
    "load": {
        "before": [
            "responseTime",
            "logger",
            "cors",
            "responses",
            "gzip"
        ],
        "order": [
        ],
        "after": [
            "parser",
            "reactapp",
            "router",
        ]
        
    },
    "settings": {
        "reactapp": {
            "enabled": true
        },
        public:{
            enabled:false,
            defaultIndex :false
        }
    }
}