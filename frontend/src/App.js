import './App.css';
import { useEffect, useState } from 'react';
import "./style/style.css"
import { Icons } from './components/icons';
import { Window } from './components/window';
import { Text } from './components/text';
import { JqTerminal } from './components/jqTerminal';
import { ImageApplication } from './components/ImageApplication';
import { Folder } from './components/folder';
import { BootSequence } from './components/bootSequence';
import { Taskbar } from './components/taskbar';
import { Helmet } from 'react-helmet';
function App() {
  const [windows, setWindows] = useState([]);
  const [commands, setCommands] = useState([]);
  const [applications, setApplications] = useState([]);
  const [backgroundImage, setBackgroundImage] = useState({});
  const [taskAppIcon, settaskAppIcon] = useState({});
  const [metaData, setMetaData] = useState({});
  const [loading, setLoading] = useState(true);
  const [booting, setBooting] = useState(true);
  useEffect(() => {
    const getData = async () => {
      try {
        const metares = await fetch("/metadata");
        if (metares.ok) {
          const bres = await metares.json();
          setMetaData(bres || {})
        }
        const appRes = await fetch("/applications");
        if (appRes.ok) {
          const apps = await appRes.json();
          setApplications(apps || [])
        }
        const backgroundres = await fetch("/backgroundimage");
        if (backgroundres.ok) {
          const backimage = await backgroundres.json();
          setBackgroundImage(backimage || {})
        }
        const appIconRes = await fetch("/taskappicon");
        if (appIconRes.ok) {
          const appIcon = await appIconRes.json();
          settaskAppIcon(appIcon || {})
        }
        const commandsRes = await fetch("/commands");
        if (commandsRes.ok)
          setCommands(await commandsRes.json() || [])

        setLoading(false)
      } catch (err) {
        setLoading(false)
        console.log(err);
      }
    }
    getData()
  }, [])

  const CloseWindow = (id) => {
    const nWindows = windows.filter((el) => el.id !== id);
    setWindows(nWindows);
  }
  const OpenWindow = (id) => {
    const windowExist = windows.find(window => window.id === id);
    if (!windowExist) {
      {
        const nWindows = JSON.parse(JSON.stringify(applications.find(el => el.id === id)));
        if (nWindows) {
          nWindows.zIndex = windows.length + 1;
          nWindows.fullScreen = false;
          nWindows.minimized = false;


          setWindows([...windows, nWindows]);
        }
      }
    }
    else if (windowExist && windowExist.minimized) {
      const nWindows = windows.map(window => {
        if (window.id == id) {
          window.minimized = false;
        }
        return window;
      })
      setWindows(nWindows);
      setActiveWindow(id);
    }
    else {
      setActiveWindow(id)
    }
  }
  const minimizeWindow = (minimize, id) => {
    const nWindows = windows.map(el => {
      if (el.id === id) {
        el.minimized = minimize;
      }
      return el;
    })
    setWindows(nWindows)
  }

  const updateFullScreen = (fullScreen, id) => {
    const nWindows = windows.map(el => {
      if (el.id === id) {
        el.fullScreen = fullScreen;
      }
      return el;
    })
    setWindows(nWindows)
  }
  const setActiveWindow = (id) => {
    if (windows.find(el => el.id === id).zIndex !== windows.length) {

      const copyWindows = JSON.parse(JSON.stringify(windows));
      const currentZindex = copyWindows.find(window => window.id === id).zIndex;
      const nWindows = copyWindows.map(window => {
        if (window.id !== id && window.zIndex > currentZindex)
          window.zIndex = Math.max(window.zIndex - 1, 1);
        else if (window.id == id)
          window.zIndex = windows.length;
        return window;
      })
      setWindows(nWindows);
    }
  }




  return (
    <div className="app">
      <Helmet>
        <title>{metaData.title || ""}</title>
        <link rel="icon" href={metaData.favicon?.url} />
      </Helmet>
      {(booting || loading) ? <BootSequence setBooting={setBooting} />
        :
        <div className="container">
          <Taskbar
            taskAppIcon={taskAppIcon}
            windows={windows}
            applications={applications}
            OpenWindow={OpenWindow}
            setWindows={setWindows}
            setActiveWindow={setActiveWindow}
          />
          <div className="desktop" style={{ backgroundImage: "url(" + backgroundImage?.image?.url + ")" }}>
            <Icons OpenWindow={OpenWindow} applications={applications} />
            <div className="windows">
              {
                windows.map((el) => {

                  switch (el.type[0].__component) {

                    case "content.terminal": {
                      return (

                        <Window key={el.id} el={el} updateFullScreen={updateFullScreen} minimizeWindow={minimizeWindow} CloseWindow={CloseWindow} setActiveWindow={setActiveWindow} >
                          <JqTerminal el={el} applications={applications} commands={commands} OpenWindow={OpenWindow} windows={windows} />
                        </Window>
                      )
                    }

                    case "content.text": {
                      return (

                        <Window key={el.id} el={el} updateFullScreen={updateFullScreen} minimizeWindow={minimizeWindow} CloseWindow={CloseWindow} setActiveWindow={setActiveWindow} >
                          <Text data={el} />
                        </Window>
                      )
                    }
                    case "content.images": {
                      return (

                        <Window key={el.id} el={el} updateFullScreen={updateFullScreen} minimizeWindow={minimizeWindow} CloseWindow={CloseWindow} setActiveWindow={setActiveWindow} >
                          <ImageApplication data={el} />
                        </Window>
                      )
                    }
                    case "content.folder": {
                      return (
                        <Window key={el.id} el={el} updateFullScreen={updateFullScreen} minimizeWindow={minimizeWindow} CloseWindow={CloseWindow} setActiveWindow={setActiveWindow} >
                          <Folder el={el} OpenWindow={OpenWindow} />
                        </Window>
                      )
                    }
                  }


                })
              }

            </div>
          </div>
        </div>
      }
    </div >

  );
}

export default App;
