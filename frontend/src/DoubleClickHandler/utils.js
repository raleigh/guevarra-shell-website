import useCancellablePromises from "./useCancelable-promise";
import { cancellablePromise } from './cancellable-promise'
const noop = () => { };

const delay = n => new Promise(resolve => setTimeout(resolve, n));

export {
    noop,
    delay,
    cancellablePromise,
    useCancellablePromises
}