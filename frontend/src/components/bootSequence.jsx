import React, { useEffect, useState } from 'react';
const $ = window.$;
export const BootSequence = ({ setBooting }) => {
    const [bootSequence, setBootSequence] = useState({});
    const [loading, setLoading] = useState(false);

    let terminal = null;
    useEffect(() => {
        const loadData = async () => {
            setLoading(true);
            const bootres = await fetch("/bootsequence");
            if (bootres.ok) {
                const bres = await bootres.json();
                setBootSequence(bres || {})
            }
            setLoading(false)
        }
        loadData()
    }, [])
    useEffect(() => {
        terminal =
            $('#terminalhere').terminal(function () {

            }, {
                greetings: false,
                name: 'terminal',
                prompt: "$> "
            });

        let i = 0,
            interval;
        if (bootSequence?.instructions && !loading) {

            interval = setInterval(() => {
                if (i < bootSequence?.instructions.length) {
                    terminal.echo(bootSequence?.instructions[i])
                    i += 1;

                }
                else {
                    setBooting(false)
                }
            }, bootSequence.delay || 200);
        }
        if (loading && !bootSequence?.instructions) {
            terminal.echo("...")
        }


        return () => {
            clearInterval(interval)
        }

    }, [bootSequence, loading])
    return (
        <div id="terminalhere" style={{ height: "100%", width: "100%" }}>

        </div>
    )
}
