import React from 'react'
// import Carousel from 'react-elastic-carousel';
import ImageGallery from 'react-image-gallery';
import "react-image-gallery/styles/css/image-gallery.css";
export const ImageApplication = ({ data, positions }) => {
    const images = data.type[0].images.map(img => {
        img.original = img.url;
        return img;
    });
    return (
        <ImageGallery items={images}
            showBullets={false}
            showFullscreenButton={false}
            showPlayButton={false}
            showThumbnails={false}
            slideDuration={10}
            infinite={false}
            renderItem={(item) => <img src={item.original} alt="not found" className="image-gallery-image" style={data.fullScreen ? { width: 'auto' } : { maxHeight: Number(positions.height.split("px")[0]) - 30 + "px", width: 'auto' }} />}
        />

    )
}
