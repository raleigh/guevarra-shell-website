import React from 'react'
import { Icon } from './icon'

export const Folder = ({ el, OpenWindow }) => {
    const applications = el.type[0].applications
    return (
        <div className="folder_icons" >
            {
                applications.map((icon) => {
                    return <Icon key={icon.id} iconData={icon} OpenWindow={OpenWindow} />
                })
            }

        </div>
    )
}
