import React from 'react'
import useClickPreventionOnDoubleClick from '../DoubleClickHandler'
import isMobile from '../utils/checkMobileDevice'

export const Icon = ({ iconData, OpenWindow }) => {
    const onClick = () => {
        if (isMobile())
            OpenWindow(iconData.id)
    }
    const onDoubleClick = () => {
        OpenWindow(iconData.id)
    }
    const [handleClick, handleDoubleClick] = useClickPreventionOnDoubleClick(onClick, onDoubleClick)

    return (
        <div className="icon" onClick={handleClick} onDoubleClick={handleDoubleClick} pointerEvents="all">
            <img src={iconData?.icon?.url} className="icon_image" />
            <span className="icon_text">

                {iconData.applicationName}
            </span>
        </div>
    )
}
