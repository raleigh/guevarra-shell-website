import React from 'react'
import { Icon } from './icon';
export const Icons = ({ OpenWindow, applications }) => {

    return (
        <div className="icons">
            {
                applications
                    .map((icon) => {
                        if (icon.showOnDesktop)
                            return <Icon key={icon.id} iconData={icon} OpenWindow={OpenWindow} />
                    })
            }

        </div>
    )
}
