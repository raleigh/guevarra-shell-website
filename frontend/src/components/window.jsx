import React, { useEffect, useRef, useState } from 'react'
import close from "../images/cancel.svg";
import minize from "../images/remove.svg";
import squares from "../images/squares.svg";
import square from "../images/square.svg";
import isMobile from '../utils/checkMobileDevice';
export const Window = ({ children, CloseWindow, el, setActiveWindow, minimizeWindow, updateFullScreen }) => {
    const [positions, setPositions] = useState({});
    const [mouseDown, setMouseDown] = useState({ active: false, pos: { left: 0, top: 0, x: 0, y: 0 } });
    const actions = useRef();
    const titlebar = useRef();
    const outerWindow = useRef();
    useEffect(() => {
        window.addEventListener("mousemove", handleMouseMove);
        window.addEventListener("mouseup", handleMouseUp);
        window.addEventListener("resize", handleResize);
        if (titlebar.current) {
            titlebar.current.addEventListener("mousedown", handleMouseDown);
        }
        if (outerWindow.current)
            outerWindow.current.addEventListener("click", handleClick);
        return () => {
            window.removeEventListener("mousemove", handleMouseMove);
            window.removeEventListener("mouseup", handleMouseUp);
            window.removeEventListener("resize", handleResize);

            if (titlebar.current) {

                titlebar.current.removeEventListener("mousedown", handleMouseDown);

            }
            if (outerWindow.current)
                outerWindow.current.removeEventListener("click", handleClick);

        }
    }, [titlebar, mouseDown, positions, el, children, outerWindow.current]);

    useEffect(() => {
        const nWindows = {};
        if (isMobile()) {
            nWindows.positions = { left: "0px", top: "0px", width: "calc(100% - 2px)", maxWidth: "600px", height: "100%" };
        }
        else {
            if (window.innerWidth > 600) {

                const left = Math.max(Math.floor(Math.random() * ((window.innerWidth - 602) - 0) + 0), 0);
                const top = Math.max(Math.floor(Math.random() * ((window.innerHeight - 402) - 0) + 0), 0);

                nWindows.positions = { left: left + "px", top: top + "px", width: "600px", maxWidth: "600px", height: "400px" };
            }
            else
                nWindows.positions = { left: "0px", top: "0px", width: "calc(100% - 2px)", maxWidth: "600px", height: "400px" };
        }

        setPositions(nWindows.positions);
    }, [])
    const handleResize = (e) => {
        e.preventDefault()
        const nPositions = JSON.parse(JSON.stringify(positions));
        const styles = getComputedStyle(outerWindow.current);
        const borderWidth = parsePx(styles.borderWidth) * 2;
        if (parsePx(styles.left) + outerWindow.current.clientWidth + borderWidth > document.documentElement.clientWidth) {
            nPositions.left = Math.max(document.documentElement.clientWidth - (outerWindow.current.clientWidth + borderWidth), 0) + 'px';
        }
        if (parsePx(styles.width) > window.innerWidth) {
            nPositions.maxWidth = `calc(100% - ${borderWidth}px)`;
            nPositions.width = `600px`;
        }
        setPositions(nPositions);
    }
    function parsePx(value) {
        return Math.abs(Number(value.replace("px", "")))
    }
    const handleMouseDown = (e) => {

        e.preventDefault()
        const left = Math.abs(Number(positions.left.replace("px", "")))
        const top = Math.abs(Number(positions.top.replace("px", "")))
        setMouseDown({ active: true, pos: { left, top, x: e.clientX, y: e.clientY } });

    }
    const handleMouseUp = (e) => {
        if (mouseDown.active) {

            setMouseDown({ active: false, pos: { x: 0 } });
        }
    }
    const handleMouseMove = (e) => {
        e.preventDefault()
        if (mouseDown.active) {
            const styles = getComputedStyle(outerWindow.current);
            const borderWidth = parsePx(styles.borderWidth) * 2,
                width = parsePx(styles.width),
                height = parsePx(styles.height);

            setPositions({
                ...positions,
                left: Math.min(Math.max(mouseDown.pos.left + (e.clientX - mouseDown.pos.x), 0), document.documentElement.clientWidth - width - borderWidth) + "px",
                top: Math.max(Math.min(mouseDown.pos.top + (e.clientY - mouseDown.pos.y), document.documentElement.clientHeight - height - 30 - borderWidth), 0) + "px"
            })
        }

    }
    const handleClick = (e) => {
        if (!actions.current.contains(e.target))
            setActiveWindow(el.id)
    }
    const childrenWithProps = React.Children.map(children, child => {
        // checking isValidElement is the safe way and avoids a typescript error too
        if (React.isValidElement(child)) {
            return React.cloneElement(child, { positions: positions });
        }
        return child;
    });


    return (
        (positions.left && positions.width && positions.top) &&
        <div className={el.minimized ? "window minimized" : (el.fullScreen) ? "window fullscreen" : "window"} ref={outerWindow} style={el.fullScreen ? { opacity: 1 } : { ...positions, zIndex: el.zIndex, opacity: 1 }}>
            <div className="window_titlebar" ref={titlebar} >
                <div ref={actions}>

                    <button className="action_icon" onClick={() => CloseWindow(el.id)}>
                        <img src={close} alt="icon" />
                    </button>
                    {
                        !isMobile() &&
                        <button className="action_icon" onClick={() => updateFullScreen(!el.fullScreen, el.id)}>
                            <img src={el.fullScreen ? squares : square} alt="icon" />
                        </button>
                    }
                    <button className="action_icon" onClick={() => minimizeWindow(true, el.id)}>
                        <img src={minize} alt="icon" />
                    </button>
                </div>
                <span className="application_name">
                    {el.applicationName}

                </span>
                <span className="application_icon">
                    <img src={el.icon?.url} alt="icon" />
                </span>
            </div>
            <div className="window_content">
                {childrenWithProps}
            </div>
        </div> || null
    )
}
