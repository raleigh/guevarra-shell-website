import React,{useState,useEffect} from 'react';
import batteryIcon from "../images/battery.svg"
import wifiIcon from "../images/wifi.svg"
import soundIcon from "../images/sound.svg"

export const Taskbar = ({taskAppIcon,windows,setWindows,setActiveWindow,applications,OpenWindow}) => {
    const [time, setTime] = useState("");
    const [date, setDate] = useState("");
    const computeTime=()=>{
        const date= new Date();
        setTime( date.toLocaleTimeString([], {timeStyle: 'short'}));
    }
    const computeDate=()=>{
        const date=new Date()
      setDate(date.toLocaleDateString('default', { weekday:"long",month: 'short' }))

    }
    useEffect(()=>{
        computeTime()
        computeDate()
        setInterval(() => {
            computeTime()
        }, 1000);
    },[])
    const handleTaskWindowClick = (e, id) => {
        if (![...e.currentTarget.querySelectorAll("ul")].some(ul => ul.contains(e.target))) {
    
          if (windows.find(el => el.id == id)) {
            const nWindows = windows.map(el => {
              if (el.id === id) {
                el.minimized = false;
              }
              return el;
            })
            setWindows(nWindows)
            setActiveWindow(id);
          }
        }
    
      }
    
    function handleChildItemClick(e, id) {
        if (![...e.currentTarget.querySelectorAll("ul")].some(ul => ul.contains(e.target))) {
          OpenWindow(id)
        }
      }
    const RenderApplications = (apps, firstItem) => {
        if (!Array.isArray(apps) || !apps.length > 0)
          return null;
        return (
          <ul style={firstItem && { left: 0, top: "100%" } || {}}>
    
            {apps.map(app => {
              if (firstItem && !app.showOnDesktop)
                return null;
              return <li key={app.id} pointerEvents="all" onClick={(e) => handleChildItemClick(e, app.id)} className={(app.type[0].__component == "content.folder") ? "taskwindow taskfolder" : "taskwindow"}>
                <img src={app.icon?.url} alt="img" className="taskimage" />
                {app.applicationName}
                {(app.type[0].__component == "content.folder") && <span className="taskbar_arrow">
                  ❯
    
                </span>}
                {
                  (app.type[0].__component == "content.folder") && RenderApplications(applications.find(el => el.id == app.id)?.type[0]?.applications, false)
                }
              </li>
            })}
          </ul>)
      }
    return (
        <div className="taskbar">
            <div className="taskwindow taskfolder">
                <img src={taskAppIcon?.image?.url} alt="icon" className="taskimage" />
          Applications
          {

                    RenderApplications(applications, true)
                }

            </div>
            {
                windows.map((window) => {
                    return (
                        <div className={`taskwindow ${(window.zIndex == windows.length) && "active"} `} key={window.id} onClick={(e) => handleTaskWindowClick(e, window.id)}>
                            <img src={window.icon?.url} alt="img" className="taskimage" />
                            {window.applicationName}

                        </div>
                    )
                })
            }
            <div className="taskbar_widgets">
                <div className="widget">
<img src={batteryIcon} alt="battery"/>
                </div>
                <div className="widget">
<img src={wifiIcon} alt="wifi"/>
                </div>
                <div className="widget">
<img src={soundIcon} alt="sound"/>
                </div>
            <div className="widget">
                {date}
            </div>
                <div className=" widget time_widget">
                   {time}
</div>
            </div>
        </div>
    )
}
