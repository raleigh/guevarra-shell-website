import React, { useCallback, useEffect, useState } from 'react';
import { Converter } from "showdown";
const converter = new Converter();
const $ = window.$;
export const JqTerminal = ({ el, OpenWindow, commands, applications, windows }) => {
    const [first, setFirst] = useState(true);
    const handle = useCallback(function (command) {
        if (command !== '') {
            const data = commands.find(d => d.commnad === command);
            if (data) {

                if (data.responseType[0].__component === "content.text") {
                    this.echo(converter.makeHtml(String(data.responseType[0].text)), { raw: true });
                }
                else if (data.responseType[0].__component === "content.applicationref" && applications.find(app => app.id === data.responseType[0].application.id)) {
                    OpenWindow(data.responseType[0].application.id);
                }
                else {
                    this.echo("Application not found: " + converter.makeHtml(command), { raw: true })
                }
            }
            else {
                this.echo("Command not found: " + command, { raw: true })
            }
        }
    }, [windows])
    let terminal = null;
    useEffect(() => {
        terminal =
            $('#terminalhere').terminal(handle, {
                greetings: false,
                name: 'terminal',
                prompt: el.type[0].prompt || "$> "
            });
        if (first) {
            setFirst(false);

            terminal.echo(converter.makeHtml(el.type[0].greetings || ""), { raw: true })

        }

        terminal.set_interpreter(handle);
        return () => {
            terminal.set_interpreter(handle);
        }

    }, [commands, OpenWindow, windows, first, applications])
    return (
        <div id="terminalhere" style={{ height: "100%", width: "100%" }}>

        </div>
    )
}
