import React from 'react'
import MarkdownPreview from '@uiw/react-markdown-preview';
export const Text = ({ data }) => {
    const value = data.type[0].text;
  
    return (
        <MarkdownPreview source={value} className="textcontainer" />

    )
}
